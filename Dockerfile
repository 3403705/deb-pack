FROM ubuntu:20.04

COPY mav-deb-pack.deb /deb/mav-deb-pack.deb

WORKDIR /deb

RUN apt-get update && apt install -y ./mav-deb-pack.deb

